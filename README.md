Docker image with PHP and [Nginx Unit](https://unit.nginx.org)

User `app` created after start container with `APP_USER_ID` environment variable, `APP_USER_ID=1001` by default.

Nginx unit state directory `/var/lib/unit`

Environment variables
 - APP_USER_ID `1001` User id for mount app volume from host
 - PHP_INI_SCAN_DIR `:/var/lib/php` can be set to override the scan directory set via the configure script
 
 ---
Environment variables for debug
 - XDEBUG_CONFIG: `remote_host=${HOST} remote_enable=1` configure XDebug
 - PHP_IDE_CONFIG: `serverName=App` To tell PhpStorm which path mapping configuration it should use for a connection from a certain machine, where ‘App’ is the name of the server configured in ‘Project Settings | PHP | Servers’
```
version: "3.3"
services:
  app:
    image: genhoi/php-unit:7.4-ubuntu
    ports:
      - 8100:8100
    volumes:
      - './config/unit:/var/lib/unit'
      - './config/php:/var/lib/php'
      - './app:/app'
    environment:
      APP_USER_ID: 1001
```